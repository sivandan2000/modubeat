﻿using UnityEngine;
using System.Collections;

public class StepSprite : MonoBehaviour {

  
    SpriteRenderer renderer;


    void Awake()
    {
        renderer = GetComponent<SpriteRenderer>();

    }



    Color tColor;
    [SerializeField]
    float speedVanish;

    IEnumerator Start()
    {
        Vector3 tPos = Vector3.zero;
        tPos.y = 0.2f;
        transform.position += tPos;
        tColor = renderer.color;

        while (renderer.color.a >= 0.02f)
        {
            

            tColor.a *= speedVanish;
            renderer.color = tColor;
            yield return null;
        }

        
        Destroy(this.gameObject);

    }
}
