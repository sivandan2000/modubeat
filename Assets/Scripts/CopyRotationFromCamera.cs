﻿using UnityEngine;
using System.Collections;

public class CopyRotationFromCamera : MonoBehaviour
{
    Camera cam;

    // Use this for initialization
    void Start()
    {
        cam = GameObject.Find("Main Camera").GetComponent<Camera>();
    }
	
	// Update is called once per frame
	void LateUpdate()
    {
		if(cam != null){
			transform.rotation = cam.transform.rotation;
		} else {
			cam = GameObject.Find("Main Camera").GetComponent<Camera>();
		}
    }
}
