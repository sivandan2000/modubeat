﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {

    public static LevelManager inst;

    [SerializeField]
    GameObject bulletPrefab;

    //[SerializeField]
    //AudioClip[] audioLoops;

    [SerializeField]
    int[][] playerShootDataLoops = new int[][]
    {
        //new int[] {0, 1, 0, 1, 0, 0, 1, 1},
        //new int[] {1, 0, 0, 1, 0, 0, 1, 1},
        //new int[] {1, 0, 0, 1, 0, 0, 1, 1},
        //new int[] {1, 0, 0, 1, 0, 0, 1, 1},

        //section 1
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        //section 2
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        //section 3
        new int[] {1, 0, 0, 1, 0, 0, 1, 1},
        new int[] {1, 0, 0, 1, 0, 0, 1, 1},
        new int[] {1, 0, 0, 1, 0, 0, 1, 1},
        new int[] {1, 0, 0, 1, 0, 0, 0, 0},
        //section 4
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        //section 5
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        //section 6
        new int[] {1, 0, 0, 1, 0, 0, 0, 0},
        new int[] {1, 0, 0, 1, 0, 0, 0, 0},
        new int[] {1, 0, 0, 1, 0, 0, 0, 0},
        new int[] {1, 0, 0, 1, 0, 0, 0, 0},
        //section 7
        new int[] {1, 0, 0, 1, 0, 0, 0, 0},
        new int[] {1, 0, 0, 1, 0, 0, 0, 0},
        new int[] {1, 0, 0, 1, 0, 0, 0, 0},
        new int[] {1, 0, 0, 1, 0, 0, 0, 0},
        //section 8
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        //section 9
        new int[] {0, 0, 0, 0, 1, 1, 0, 0},
        new int[] {0, 0, 0, 0, 1, 1, 1, 1},
        new int[] {0, 0, 0, 0, 1, 1, 0, 0},
        new int[] {0, 0, 0, 0, 1, 1, 1, 1},
        //section 10
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        //section 11
        new int[] {1, 0, 0, 0, 0, 0, 0, 0},
        new int[] {1, 0, 0, 0, 0, 0, 0, 0},
        new int[] {1, 0, 0, 0, 0, 0, 0, 0},
        new int[] {1, 0, 0, 0, 0, 0, 0, 0},
        //section 12
        new int[] {0, 0, 0, 0, 1, 1, 0, 0},
        new int[] {0, 0, 0, 0, 1, 1, 1, 1},
        new int[] {0, 0, 0, 0, 1, 1, 0, 0},
        new int[] {0, 0, 0, 0, 1, 1, 1, 1},
        //section 13
        new int[] {0, 0, 0, 0, 1, 1, 0, 0},
        new int[] {0, 0, 0, 0, 1, 1, 1, 1},
        new int[] {0, 0, 0, 0, 1, 1, 0, 0},
        new int[] {0, 0, 0, 0, 1, 1, 1, 1},
        //section 14
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        //section 15
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},




        //song has ended
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
    };


    [SerializeField]
    int[][] spawnDataLoops = new int[][]
    {
        // 1 = 1
        // 2 = 2
        // 3 = 1 + 2
        // 4 = 3
        // 5 = 1 + 3
        // 6 = 1 + 3
        // 7 = 2 + 3
        // 8 = 4
        // 15 = 1 + 2 + 3 + 4

        //monster tests
        /*
        new int[] {1, 2, 8, 1, 4, 2, 8, 4},
        new int[] {1, 2, 8, 1, 4, 2, 8, 4},
        new int[] {1, 2, 8, 1, 4, 2, 8, 4},
        new int[] {1, 2, 8, 1, 4, 2, 8, 4},
        */

        //new int[] {2, 0, 0, 2, 0, 0, 2, 2},
        //new int[] {2, 0, 0, 2, 0, 0, 2, 2},
        //new int[] {2, 0, 0, 2, 0, 0, 0, 0},
        //new int[] {2, 0, 0, 2, 0, 0, 0, 0},

        //section 1
        new int[] {1, 0, 0, 0, 0, 0, 0, 0},
        new int[] {1, 0, 0, 0, 0, 0, 0, 0},
        new int[] {1, 0, 0, 0, 0, 0, 0, 0},
        new int[] {1, 0, 0, 0, 0, 0, 0, 0},
        //section 2
        new int[] {1, 0, 0, 0, 1, 0, 0, 0},
        new int[] {1, 0, 0, 0, 1, 0, 0, 0},
        new int[] {1, 0, 1, 0, 1, 0, 1, 0},
        new int[] {1, 0, 0, 0, 0, 0, 0, 0},
        //section 3
        new int[] {2, 0, 0, 2, 0, 0, 2, 2},
        new int[] {2, 0, 0, 2, 0, 0, 2, 2},
        new int[] {2, 0, 0, 2, 0, 0, 0, 0},
        new int[] {2, 0, 0, 2, 0, 0, 0, 0},
        //section 4
        new int[] {1, 0, 0, 1, 0, 0, 0, 0},
        new int[] {1, 0, 0, 1, 0, 0, 0, 0},
        new int[] {1, 1, 0, 1, 0, 0, 0, 0},
        new int[] {1, 0, 0, 1, 0, 0, 0, 0},
        //section 5
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        //section 6
        new int[] {2, 0, 0, 2, 0, 0, 2, 2},
        new int[] {2, 0, 0, 2, 0, 0, 2, 2},
        new int[] {2, 0, 0, 2, 0, 0, 2, 2},
        new int[] {2, 0, 0, 2, 0, 0, 2, 2},
        //section 7
        new int[] {3, 0, 0, 2, 0, 0, 0, 0},
        new int[] {3, 0, 0, 2, 0, 0, 0, 0},
        new int[] {3, 0, 0, 2, 0, 0, 0, 0},
        new int[] {3, 0, 0, 2, 0, 0, 0, 0},
        //section 8
        new int[] {1, 0, 0, 0, 1, 0, 0, 0},
        new int[] {1, 0, 0, 0, 1, 0, 0, 0},
        new int[] {1, 0, 0, 0, 1, 0, 0, 0},
        new int[] {1, 0, 0, 0, 1, 0, 0, 0},
        //section 9
        new int[] {4, 0, 0, 0, 4, 0, 0, 0},
        new int[] {4, 0, 0, 0, 4, 0, 0, 0},
        new int[] {4, 0, 0, 0, 4, 0, 0, 0},
        new int[] {4, 0, 0, 0, 4, 0, 0, 0},
        //section 10
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        //section 11
        new int[] {8, 0, 0, 0, 8, 0, 0, 0},
        new int[] {8, 0, 0, 0, 8, 0, 0, 0},
        new int[] {8, 0, 0, 8, 0, 0, 8, 0},
        new int[] {8, 0, 0, 8, 0, 0, 8, 0},
        //section 12
        new int[] {12, 0, 0, 0, 12, 0, 1, 0},
        new int[] {12, 0, 0, 0, 12, 0, 1, 0},
        new int[] {12, 0, 0, 0, 12, 0, 1, 0},
        new int[] {12, 0, 0, 0, 12, 0, 1, 0},
        //section 13
        new int[] {4, 0, 0, 0, 0, 0, 0, 0},
        new int[] {6, 0, 0, 2, 0, 0, 0, 0},
        new int[] {4, 0, 0, 0, 0, 0, 0, 0},
        new int[] {6, 0, 0, 2, 0, 0, 0, 0},
        //section 14
        new int[] {1, 0, 1, 0, 1, 0, 1, 0},
        new int[] {3, 0, 0, 2, 0, 0, 0, 0},
        new int[] {1, 0, 1, 0, 1, 0, 1, 0},
        new int[] {3, 0, 0, 2, 0, 0, 0, 0},
        //section 15
        new int[] {9, 0, 1, 0, 9, 0, 1, 0},
        new int[] {8, 0, 0, 0, 0, 0, 8, 0},
        new int[] {9, 8, 1, 0, 1, 8, 1, 0},
        new int[] {8, 0, 0, 8, 0, 0, 0, 0},




        //song has ended
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {16, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 32},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
        new int[] {0, 0, 0, 0, 0, 0, 0, 0},
    };

    [SerializeField]
    int[][][] danceDataLoops = new int[][][]
    {
        new int[][]
        {
            /*
            new int[] {2, 2, 2, 2, 2, 2, 2, 2},
            new int[] {2, 2, 2, 2, 2, 2, 2, 2},
            new int[] {2, 2, 2, 2, 2, 2, 2, 2},
            new int[] {2, 2, 2, 2, 2, 2, 2, 2},
            new int[] {2, 2, 2, 2, 2, 2, 2, 2},
            new int[] {2, 2, 2, 2, 2, 2, 2, 2},
            new int[] {2, 2, 2, 2, 2, 2, 2, 2},
            new int[] {2, 2, 2, 2, 2, 2, 2, 2},
            new int[] {2, 2, 2, 2, 2, 2, 2, 2},
            new int[] {2, 2, 2, 2, 2, 2, 2, 2},
            new int[] {2, 2, 2, 2, 2, 2, 2, 2},
            */
            new int[] {1, 0, 1, 0, 1, 0, 1, 0},
        },
        new int[][]
        {
            new int[] {2, 0, 0, 2, 0, 0, 1, 3},
        },
        new int[][]
        {
            new int[] {2, 0, 1, 0, 1, 1, 0, 0},
            new int[] {2, 0, 1, 0, 2, 2, 2, 2},
        },
        new int[][]
        {
            new int[] {4, 4, 5, 4, 5, 5, 4, 5},
            new int[] {4, 4, 5, 4, 5, 5, 4, 5},
            //new int[] {4, 4, 5, 5, 4, 4, 4, 4},
            //new int[] {5, 5, 4, 4, 5, 5, 5, 5},
        },
    };

    // Use this for initialization
    void Start () {
        inst = this;
        BitManager.S.Shminit += Shminit;
        startPlayMusic();
        character = GameManager.S.character.GetComponent<Character>();
        player = GameManager.S.character;
    }

    [SerializeField]
    Character character;

    GameObject player;

    [SerializeField]
    Image coolDownGlow;

    private void Shminit()
    {

        //play music
        {
            //Debug.Log("LevelManager " + BitManager.S.beatCount);
            if (BitManager.S.beatCount == 0)
            {
                //startPlayMusic();
            }
            if (BitManager.S.isLoopBeginning)
            {
                //startPlayMusic();
            }
        }

        //laser shooting from player
        {
            int command = getCurrentSpawnCommand(playerShootDataLoops, BitManager.S.loopNumber);

            if (command == 1) {
                //EnemySpawner.inst.spawnEnemy(0);

                //GameObject player = GameObject.Find("Character");

                GameManager.S.informOfPlayerShooting();
                AudioManager.S.playPlayerGunSound();

                GameObject bullet = Instantiate(bulletPrefab, player.transform.position + player.transform.forward * 1.5f + new Vector3(0,0.4f,0)/*new Vector3(1.5f, 0, 0)/* + transform.forward * 1.5f*/, Quaternion.identity) as GameObject;
                bullet.GetComponent<Bullet>().SetDirection(new Vector3(1, 0, 0));
                //float ang = player.transform.eulerAngles.y;
                //bullet.GetComponent<Bullet>().SetDirection(new Vector3(Mathf.Sin(ang) / Mathf.PI * 180, 0, Mathf.Cos(ang) / Mathf.PI * 180).normalized);
            }
        }

        //spwan shit
        {
            int command = getCurrentSpawnCommand(spawnDataLoops, BitManager.S.loopNumber);
            int enemyType = command - 1;

            if ((command & 1) > 0)
            {
                EnemySpawner.inst.spawnEnemy(0);
            }
            if ((command & 2) > 0)
            {
                EnemySpawner.inst.spawnEnemy(1);
            }
            if ((command & 4) > 0)
            {
                EnemySpawner.inst.spawnEnemy(2);
            }
            if ((command & 8) > 0) {
                EnemySpawner.inst.spawnEnemy(3);
            }
            if ((command & 16) > 0) {
                //You win the level yayyy!@#!#@!!!!!111
                GameObject.Find("YouWin").GetComponent<UnityEngine.UI.Image>().enabled = true;
            }
            if ((command & 32) > 0) {
                UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
            }

        }

        //dance shit
        {
            foreach (GameObject enemy in EnemySpawner.inst.enemies)
            {
                EnemyDancer dancer = enemy.GetComponent<EnemyDancer>();
                int command = getCurrentDanceCommand(danceDataLoops, dancer.enemyType);
                if (command > 0)
                {
                    dancer.performDanceMove(command);
                }
                
            }
        }
        
    }
    
    private static int getCurrentSpawnCommand(int[][] dataLoops, int sectionNumber)
    {
        int[] loop = dataLoops[sectionNumber];
        return loop[BitManager.S.beatInLoop];
    }

    private static int getCurrentDanceCommand(int[][][] dataLoops, int enemyType)
    {
        int[][] loops = dataLoops[enemyType];
        int[] loop = loops[Mathf.FloorToInt(BitManager.S.beatCount / 8) % loops.Length];
        return loop[BitManager.S.beatInLoop];
    }

    private void startPlayMusic()
    {
        //Debug.Log("playing the music");
        //GetComponent<AudioSource>().PlayOneShot(audioLoops[0]);
        GetComponent<AudioSource>().Play();
    }


    void Update () {
        GameObject.Find("filler").GetComponent<Image>().fillAmount = character.cooldownPercent;//UnityEngine.Random.Range(0f, 1f);
        if (character.cooldownPercent >= 1) coolDownGlow.enabled = true; else coolDownGlow.enabled = false;
    }
}
