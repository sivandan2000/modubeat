﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Tower : MonoBehaviour, IDamagble {

    public AudioSource audioSource;

    public List<int> beatOrder = new List<int>();

    public int startingBeat;
    public int beatOffset;

    public float lifeTime = 29f;
    

	protected virtual void Start () {

        BitManager.S.Shminit += Shminit;

       countBeat = -1;
        startingBeat = ((BitManager.S.beatCount + 1) % 8);
      //  countBeat = startingBeat;

        Debug.Log(transform.name + " startingBeat = " + startingBeat);
        beatOffset = startingBeat % 8;
        Debug.Log("beatOffset = " + beatOffset);
        birthTime = Time.time;
        StartCoroutine(ScaleToMax());
	}

    IEnumerator ScaleToMax()
    {
        Vector3 _scale = transform.localScale;
        transform.localScale *= 0.1f;
        while (transform.localScale.magnitude < 0.95f)
        {
            transform.localScale *= 1.2f;

            yield return null;
        }
        transform.localScale = _scale;
    }

    protected virtual void Shminit()
    {

    }

    float birthTime;

    bool isDead = false;

	protected virtual void Update () {

        if (!isDead)
        if (birthTime + lifeTime < Time.time)
        {
            StartCoroutine(DestroyTower());
                
            isDead = true;
        }


	}

    [SerializeField]
    AudioClip deathClip;

    [SerializeField]
    float _maxHealth;
    public float maxHealth
    {
        get
        {
            return _maxHealth;
        }
    }


    float _health;
    public float health
    {
        get
        {
            return _health;
        }

        set
        {
            _health = value;
        }
    }

    public float healthPercent
    {
        get
        {
            return (_health / _maxHealth);
        }
    }

    protected int countBeat;

    protected void CountBeat()
    {
        countBeat++;
        if (countBeat > 7) countBeat -= 8;

    }

    IEnumerator DestroyTower()
    {
        StartCoroutine(ScaleToZero());

        BitManager.S.Shminit -= Shminit;

        AudioSource audio = GetComponentInChildren<AudioSource>();
        Debug.Log(audio.name);
        audio.clip = deathClip;
        audio.Play();

        while (audio.isPlaying) yield return null;
        Destroy(this.gameObject);
        yield return null;
    }

    IEnumerator ScaleToZero()
    {
        while (transform.localScale.magnitude > 0.05f)
        {
            transform.localScale *= 0.98f;

            yield return null;
        }
    }

    public void InflictDamage(float damage)
    {
        health -= damage;
    }

    public bool isAlive { get { return health > 0; } }
}
