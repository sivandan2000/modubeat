﻿using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour {

    public float speed;
    public float coolDownTime = 4f;


    CharacterController controller;

    [SerializeField]
    GameObject pulseCannonPrefab;
    [SerializeField]
    GameObject laserCannonPrefab;

    void Awake()
    {
        controller = GetComponent<CharacterController>();

    }

   // bool makePulse = false;

    void Start()
    {
        BitManager.S.Shminit += Shminit;
    }

    bool canCreateTower = false;
    float _time;

    Vector3 lookAt;

    [SerializeField]
    Animator anim;

    public float cooldownPercent { get { return _time / coolDownTime; } }

	void Update () {

        transform.GetChild(0).localPosition = new Vector3(0, -0.005f, 0);

        lookAt = transform.position + controller.velocity;
        lookAt.y = transform.position.y;
        transform.LookAt(lookAt);

        if (!canCreateTower)
        {
            _time += Time.deltaTime;
            if (_time > coolDownTime) canCreateTower = true;
        }

        

        Vector3 horizontal = (new Vector3(0, 0, 1) * Input.GetAxis("Vertical") * speed);
        Vector3 vertical = (new Vector3(1, 0, 0) * Input.GetAxis("Horizontal") * speed);
        Vector3 moveVector = horizontal + vertical + Vector3.down * speed;


        controller.Move(moveVector * Time.deltaTime);
       // Debug.Log("mag = " + controller.velocity.magnitude);
        anim.SetFloat("speed", controller.velocity.magnitude);

        if (canCreateTower)
        {
            if (Input.GetKeyDown(KeyCode.Z) || Input.GetButtonDown("Fire1"))
            {
                canCreateTower = false;
                _time = 0;
                Debug.Log("z");
                // makePulse = true;
                StartCoroutine(MakePulseCannon());
            }
            if (Input.GetKeyDown(KeyCode.X) || Input.GetButtonDown("Fire2"))
            {
                canCreateTower = false;
                _time = 0;
                Debug.Log("z");
                // makePulse = true;
                StartCoroutine(MakeLaserCannon());
            }
        }


    }

    bool shminit = false;

    void Shminit()
    {
        shminit = true;
    }

    Vector3 backDirection = new Vector3(-2.4f, 0, 0);

    IEnumerator MakePulseCannon()
    {
        while (!shminit) yield return null;
        
        GameObject pulse = Instantiate(pulseCannonPrefab, transform.position + backDirection, transform.rotation) as GameObject;
        shminit = false;
    }

    IEnumerator MakeLaserCannon()
    {
        while (!shminit) yield return null;

        GameObject laser = Instantiate(laserCannonPrefab, transform.position + backDirection, transform.rotation) as GameObject;
        shminit = false;
    }
}
