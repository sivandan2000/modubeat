﻿using UnityEngine;
using System.Collections;

public class Pulse : MonoBehaviour {

    public Vector3 maxScale;
    [SerializeField]
    float lifeTime;

    Vector3 _scale;
    [SerializeField]
    float expandFactor;

	IEnumerator Start ()
    {
        _scale = transform.localScale;
        while (transform.localScale.x < maxScale.x - 0.05f)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, maxScale, expandFactor * Time.deltaTime);
            
            yield return null;
        }

        yield return new WaitForSeconds(lifeTime);
        yield return StartCoroutine(ScaleToZero());
        Destroy(this.gameObject);
	}

    IEnumerator ScaleToZero()
    {
      //  while (transform.localScale.magnitude > 0.05f)
      //  {
      //      transform.localScale *= 0.5f;

            yield return null;
      //  }
    }

    public float damage = 5f;

    void OnTriggerEnter(Collider coll)
    {
        Debug.Log("coll!");
        IDamagble d = coll.GetComponent<IDamagble>();
        if (d != null) coll.GetComponent<IDamagble>().InflictDamage(damage);

    }
}
