﻿using UnityEngine;
using System.Collections;
using System;

public class EnemyDancer : Enemy {

    private Vector3 myVel = new Vector3();
    public int enemyType;

    protected override void Start()
    {
        base.Start();
        transform.eulerAngles = new Vector3(0, -90, 0);
    }

   
    protected override void Update()
    {
        base.Update();
    }


    void FixedUpdate () {
        transform.position += myVel;
        myVel *= 0.8f;
        transform.eulerAngles = new Vector3(0, -90 + (Mathf.Sin(myVel.z*15) / Mathf.PI * 180), 0);

        bool shouldIdle = (myVel.magnitude < 0.01f);

        if (shouldIdle && anim.GetCurrentAnimatorStateInfo(0).IsName("mutant_walking"))
        {
            anim.Play("mutant_breathing_idle");
        }
        if (!shouldIdle && anim.GetCurrentAnimatorStateInfo(0).IsName("mutant_breathing_idle"))
        {
            anim.Play("mutant_walking");
        }



        //  Debug.Log("monsterVel = " + myVel.magnitude);
        //  anim.SetFloat("speed", myVel.magnitude * 100);
        // transform.LookAt(transform.forward);
    }

    internal void performDanceMove(int command)
    {
        float dir;
        switch (command)
        {
            case 1:
                myVel = new Vector3(-0.111f, 0, 0);
                break;
            case 2:
                myVel = new Vector3(-0.155f, 0, 0);
                break;
            case 3:
                dir = (Mathf.Round(UnityEngine.Random.Range(0f, 1f)) - 0.5f) * 2;
                if (transform.position.z < (GameManager.S.minY + 4)) dir = 1;
                if (transform.position.z > (GameManager.S.maxY - 4)) dir = -1;
                myVel = new Vector3(-0.24f, 0, dir * 0.34f);
                break;
            case 4:
                dir = 1;
                if (transform.position.z < (GameManager.S.minY + 4)) dir = 1;
                if (transform.position.z > (GameManager.S.maxY - 4)) dir = -1;
                myVel = new Vector3(-0.12f, 0, dir * 0.26f );
                break;
            case 5:
                dir = -1;
                if (transform.position.z < (GameManager.S.minY + 4)) dir = 1;
                if (transform.position.z > (GameManager.S.maxY - 4)) dir = -1;
                myVel = new Vector3(-0.12f, 0, dir * 0.26f);
                break;
            default:
                break;
        }
    }

    protected override void playDeathSound ()
    {
        AudioManager.S.playEnemyDeathSound(enemyType);
    }

}
