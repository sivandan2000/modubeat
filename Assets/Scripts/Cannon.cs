﻿using UnityEngine;
using System.Collections;
using System;

public class Cannon : Tower {

    [SerializeField]
    GameObject bulletPrefab;

    public int fireBeat;

    [SerializeField]
    float forceAmount;

    [SerializeField]
    GameObject pulsePrefab;

    int firstBeat;

    protected override void Start()
    {
        base.Start();
        MakePulse();

        GameManager.S.cannonList.Add(this);

        Vector3 pos = transform.position;
        pos.y = 0.031f;
        transform.position = pos;

     //   BitManager.S.Shminit += Shminit;

        /*  beatOrder.Insert(0,1);
          beatOrder.Insert(0, 0);
          beatOrder.Insert(0, 0);
          beatOrder.Insert(0, 0);
          beatOrder.Insert(0, 0);
          beatOrder.Insert(0, 0);
          beatOrder.Insert(0, 0);
          beatOrder.Insert(0, 0);
          */

        beatOrder.Add(1);
        beatOrder.Add(0);
        beatOrder.Add(0);
        beatOrder.Add(0);
        beatOrder.Add(0);
        beatOrder.Add(0);
        beatOrder.Add(0);
        beatOrder.Add(0);

    }

    protected override void Update()
    {
        base.Update();
    }

    void DoAction(int beat)
    {
     //   Debug.Log(14 % 8);
       // if (beat == 1)
       if (countBeat >= 0)
       if ((countBeat + 8) % 8 == 0)
            MakePulse();

    }

    void MakePulse()
    {
        // Debug.Log("Pulse  on beat " + BitManager.S.beatCount);
        //audioSource.Play();
        AudioManager.S.PlayAudioSource(countBeat);//_action);
        //GameObject pulse = Instantiate(pulsePrefab, transform.position, Quaternion.identity, transform) as GameObject;
        StartCoroutine(makePulses());
        innerPart.transform.localPosition = Vector3.zero;

    }

    [SerializeField]
    float timeBetweenPulses = 0.05f;

    [SerializeField]
    int numberOfPulses = 3;
    IEnumerator makePulses()
    {
        for (int i = 0; i < numberOfPulses; i++)
        {
            GameObject pulse = Instantiate(pulsePrefab, transform.position, Quaternion.identity, transform) as GameObject;
            yield return new WaitForSeconds(timeBetweenPulses);
        }
    }


    int _action;

    protected override void Shminit()
    {
        CountBeat();
        _action = (BitManager.S.beatCount) % 8;// + beatOffset;
     //   beatOrder[(BitManager.S.beatCount) % 8] = 1;
        if (_action > 7) _action -= 8;
        //        Debug.Log("sending beat " + BitManager.S.beatCount % 8);
        DoAction(countBeat);//beatOrder[countBeat]);
        Debug.Log(transform.name  + "CountBeat = " + countBeat + "BeatMain = " + (BitManager.S.beatCount) % 8);//_action]);
    }

    [SerializeField]
    GameObject innerPart;

    public void FireBullet()
    {
        GameObject bullet = Instantiate(bulletPrefab,transform.position, Quaternion.identity) as GameObject;
        bullet.GetComponent<Rigidbody>().AddForce(new Vector3(forceAmount, 0, 0));


    }

}
