﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour {

    
    public GameObject characterPrefab;

    public static GameManager S;

    public List<Cannon> cannonList = new List<Cannon>();
    public List<LaserCannon> laserCannonList = new List<LaserCannon>();

    public float minY;
    public float maxY;

    public int enemiesReachedEnd;

    public Action EnemyReachedEndEvent;

    public GameObject character;

    void Awake()
    {
        S = this;
        EnemyReachedEndEvent += EnemyReachedEnd;
        character = Instantiate(characterPrefab) as GameObject;
    }

    void Start()
    {
        playerAnim = character.GetComponentInChildren<Animator>();
    }

    [SerializeField]
    int maxEnemiesToReachEnd = 3;

    void EnemyReachedEnd()
    {
        Debug.Log("Enemy reached left");
        enemiesReachedEnd++;
        AudioManager.S.playerLoseHeartSound.Play();


        GameObject.Find("LifeHeart1").GetComponent<UnityEngine.UI.Image>().enabled = (enemiesReachedEnd < 3);
        GameObject.Find("LifeHeart2").GetComponent<UnityEngine.UI.Image>().enabled = (enemiesReachedEnd < 2);
        GameObject.Find("LifeHeart3").GetComponent<UnityEngine.UI.Image>().enabled = (enemiesReachedEnd < 1);

        if (enemiesReachedEnd >= maxEnemiesToReachEnd) {
            playerAnim.SetTrigger("playerDeath");
            character.GetComponent<CharacterController>().enabled = false;
            Debug.Log("Player LOOSE!");
            AudioManager.S.playerDeathSound.Play();

            StartCoroutine(PlayerLoose());
        }
    }

    public void informOfPlayerShooting()
    {
        playerAnim.SetTrigger("shoot");
    }

   // [SerializeField]
    Animator playerAnim;
    [SerializeField]
    GameObject player;

    IEnumerator PlayerLoose()
    {
        yield return new WaitForSeconds(6f);

        SceneManager.LoadScene("MainMenu");

        yield return null;
    }

}
