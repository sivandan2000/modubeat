﻿using UnityEngine;
using System.Collections;
using System;

public class BitManager : MonoBehaviour
{
    public static BitManager S;

    public Action Shminit;
    

    public float bpm = 120;
    public int beatCount = 0;

    public int beatInLoop { get { return beatCount % 8; } }
    public int loopNumber { get { return Mathf.FloorToInt(beatCount / 8); } }
    public bool isLoopBeginning { get { return beatInLoop == 0; } }

    InputManager IM;

    void Awake()
    {
        S = this;



    }

    void Start()
    {
        IM = InputManager.S;
        if (Shminit != null) Shminit();

    }


    void FixedUpdate()
    {
        CountBeats();

        //if (Time.time >= beatCount * 60 / bpm / 2) RunShminiot();

    }

    float _time;

    float currentBeatTime;

    void RunShminiot()
    {
        if (IM.action == 1)
            foreach (Cannon cannon in GameManager.S.cannonList)
                cannon.FireBullet();


        IM.action = 0;

    }

    [SerializeField]
    //float timeOffset = 0.025f;
    float timeOffset = 0.08f;

    void CountBeats()
    {
        
      //  _time += Time.fixedDeltaTime;

        if ( LevelManager.inst.GetComponent<AudioSource>().time >= (beatCount * 60 / (bpm*2)) - timeOffset )
        {
            if (Shminit != null) Shminit();

            beatCount++;
           // Debug.Log(beatCount);
          //  Debug.Log(Time.time);//bitCount);
                                 //  Debug.Log(_time);

            //RunShminiot();
        }
        
    }
    


}