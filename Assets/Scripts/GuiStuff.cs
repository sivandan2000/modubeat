﻿using UnityEngine;
using System.Collections;

public class GuiStuff : MonoBehaviour {

    public Texture aTexture;

    void OnGUI()
    {
        if (!aTexture)
        {
            Debug.LogError("Assign a Texture in the inspector.");
            return;
        }
        GUI.DrawTexture(new Rect(10, 10, 60, 60), aTexture, ScaleMode.ScaleAndCrop, true, 1.0F);
    }

    // Use this for initialization
    void Start()
    {

    }
    
    // Update is called once per frame
    void Update () {
	
	}
}
