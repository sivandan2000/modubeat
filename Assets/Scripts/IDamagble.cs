﻿using UnityEngine;
using System.Collections;

public interface IDamagble {
    
    float maxHealth { get; }
    float health { get; set; }
    float healthPercent { get; }

    void InflictDamage(float damage);

    bool isAlive { get; }


}
