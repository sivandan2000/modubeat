﻿using UnityEngine;
using System.Collections;

public class InnerPart : MonoBehaviour {

    Vector3 pos;

    [SerializeField]
    float movementFactor = 1.011f;

    IEnumerator Start () {
        while (true)
        {
            pos = transform.position;
            pos.y *= movementFactor; 
            transform.position = pos;

            yield return null;
        }
	}
	
}
