﻿using UnityEngine;
using System.Collections;
using System;
public class Enemy : MonoBehaviour, IDamagble {

    [SerializeField]
    GameObject stepSprite;

    [SerializeField]
    GameObject deathSprite;

    [SerializeField]
    GameObject instantiateMonster;

    [SerializeField]
    float _maxHealth;
    public float maxHealth
    {
        get
        {
            return _maxHealth;
        }
    }


    float _health;
    public float health
    {
        get
        {
            return _health;
        }

        set
        {
            _health = value;
        }
    }

    public float healthPercent
    {
        get
        {
            return (_health / _maxHealth);
        }
    }

    public bool isAlive
    {
        get
        {
            return health > 0;
        }
    }

    AudioSource deathAudio;
    // Use this for initialization
    protected virtual void Start () {

        deathAudio = AudioManager.S.deathAudio;
        BitManager.S.Shminit += Shminit;

        health = maxHealth;

        GameObject _instantiateMonster = Instantiate(instantiateMonster, transform.position, Quaternion.identity, transform) as GameObject;

    }

    //[SerializeField]
    float leftBorder = -19;






    protected virtual void Update () {

        if (transform.position.x < leftBorder)
        {
            if (GameManager.S.EnemyReachedEndEvent != null) GameManager.S.EnemyReachedEndEvent();
            KillEnemy();
        }
	}


    public void InflictDamage(float damage)
    {
        if (health > 0) {
            health -= damage;
            if (damage > 0) {
                Vector3 spritePos = transform.position + new Vector3(0, 2.2f, 0);
                GameObject _deathSprite = Instantiate(deathSprite, spritePos, Quaternion.identity) as GameObject;

                if (!isAlive) playDeathSound();
                if (!isAlive) StartCoroutine(DestroyEnemy());
            }
        }
    }


    void KillEnemy()
    {
        BitManager.S.Shminit -= Shminit;

        if (EnemySpawner.inst.enemies.Contains(this.gameObject)) EnemySpawner.inst.enemies.Remove(this.gameObject);
        Destroy(this.gameObject);
    }

    [SerializeField]
    public Animator anim;


    IEnumerator DestroyEnemy()
    {

        EnemySpawner.inst.enemies.Remove(this.gameObject);

        //Vector3 spritePos = transform.position + new Vector3(0, 2.2f, 0);
        //GameObject _deathSprite = Instantiate(deathSprite, spritePos, Quaternion.identity) as GameObject;

        anim.SetTrigger("isDead");


        //BitManager.S.Shminit += Shminit;
        while (!shminit) yield return null;

        deathAudio.Play();
        yield return new WaitForSeconds(1f);

        //Destroy(_deathSprite.gameObject);

        BitManager.S.Shminit -= Shminit;

        Destroy(this.gameObject);


        yield return null;
    }

    protected virtual void playDeathSound ()
    {
        
    }

    bool shminit = false;

    void Shminit()
    {
        GameObject _stepSprite = Instantiate(stepSprite, transform.position, Quaternion.identity) as GameObject;
        shminit = true;
    }

}
