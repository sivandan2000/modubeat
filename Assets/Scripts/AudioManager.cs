﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {

    public static AudioManager S;
    public AudioSource[] audioPulseSources = new AudioSource[8];
    public AudioSource[] audioLaserSources = new AudioSource[8];

    public AudioSource playerGunSound;

    public AudioSource playerLoseHeartSound;
    public AudioSource playerDeathSound;

    public AudioSource[] enemyDeathSounds = new AudioSource[4];

    void Awake ()
    {
        S = this;
    }

    public void PlayAudioSource(int beat)
    {
        audioPulseSources[beat].Stop();
        audioPulseSources[beat].Play();
    }

    [SerializeField]
    AudioClip[] laserClips = new AudioClip[8];

    bool clipChanged = false;

    public void PlayLaserAudioSource(int beat, float height)
    {
        height += GameManager.S.minY;
        height /= (GameManager.S.minY - GameManager.S.maxY);
        height = Mathf.FloorToInt(height * 8);
        height--;

        height = 8 - height;

        Debug.Log(" height = " + height);

        for (int i = 1; i <=8; i++)
        {
            //int range = Mathf.FloorToInt(height / yRange);
           // Debug.Log("range = " + range);
            if (i == height)
            {
                if (audioLaserSources[beat].clip != laserClips[i - 1])
                {
                    clipChanged = true;
                    audioLaserSources[beat].clip = laserClips[i - 1];
                    if ((beat % 2) == 0) {
                        audioLaserSources[beat].volume = 0.6f;
                    }
                    break;
                }
            }
        }


        if (!clipChanged) audioLaserSources[beat].Stop();
        audioLaserSources[beat].Play();
        clipChanged = false;
    }

    float yRange;

    void Start () {

        yRange = (GameManager.S.maxY - GameManager.S.minY) / 8;
        //Camera.main.pi Screen.height
       // Debug.Log("yRange = " + yRange);
    }

    public AudioSource deathAudio;

    public void playPlayerGunSound ()
    {
        playerGunSound.Play();
    }
    public void playEnemyDeathSound(int type)
    {
        enemyDeathSounds[type].Play();
    }
}
