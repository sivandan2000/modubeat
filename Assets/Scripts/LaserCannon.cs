﻿using UnityEngine;
using System.Collections;
using System;

public class LaserCannon : Tower
{

    [SerializeField]
    GameObject bulletPrefab;

    public int fireBeat;

    [SerializeField]
    float forceAmount;

    [SerializeField]
    GameObject pulsePrefab;


    protected override void Start()
    {
        base.Start();
        GameManager.S.laserCannonList.Add(this);

        Vector3 pos = transform.position;
        pos.y = 0.82f;
        transform.position = pos;

        Vector3 _lookAt;
        _lookAt = transform.position;
        transform.LookAt(new Vector3(_lookAt.x+1, _lookAt.y, _lookAt.z));
        //   BitManager.S.Shminit += Shminit;

        /*  beatOrder.Insert(0,1);
          beatOrder.Insert(0, 0);
          beatOrder.Insert(0, 0);
          beatOrder.Insert(0, 0);
          beatOrder.Insert(0, 0);
          beatOrder.Insert(0, 0);
          beatOrder.Insert(0, 0);
          beatOrder.Insert(0, 0);
          */

        beatOrder.Add(1);
        beatOrder.Add(0);
        beatOrder.Add(0);
        beatOrder.Add(0);
        beatOrder.Add(0);
        beatOrder.Add(0);
        beatOrder.Add(0);
        beatOrder.Add(0);

    }

    protected override void Update()
    {
        base.Update();
    }

    void DoAction(int beat)
    {
        //if (beat == 1)
        if (countBeat >= 0)
            if ((countBeat + 8) % 8 == 0)

                FireBullet();

    }

    void MakePulse()
    {
        // Debug.Log("Pulse  on beat " + BitManager.S.beatCount);
        //audioSource.Play();
        AudioManager.S.PlayAudioSource(_action);
        GameObject pulse = Instantiate(pulsePrefab, transform.position, Quaternion.identity, transform) as GameObject;

    }
    int _action;

    protected override void Shminit()
    {
        CountBeat();
        _action = (BitManager.S.beatCount) % 8 + beatOffset;
        if (_action > 7) _action -= 8;
        //        Debug.Log("sending beat " + BitManager.S.beatCount % 8);
        DoAction(countBeat); //beatOrder[_action]);
    }


    public void FireBullet()
    {
        AudioManager.S.PlayLaserAudioSource(/*_action*/countBeat, transform.position.z);

        Vector3 pos = transform.position;
        pos.y += 0.5f;
        GameObject bullet = Instantiate(bulletPrefab, transform.position + new Vector3(1.5f, 0, 0)/* + transform.forward * 1.5f*/, Quaternion.identity) as GameObject;
        bullet.GetComponent<Bullet>().SetDirection(new Vector3(1, 0, 0));


    }

}
