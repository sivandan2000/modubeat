﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class EnemySpawner : MonoBehaviour {

    static public EnemySpawner inst;

    [SerializeField]
    GameObject[] enemyPrefabs;

    private Vector3 spawnLine1;           // = new Vector3(-7f, 0, 10);
    private Vector3 spawnLine2;   // = new Vector3(12, 0, 0);

    
    public List<GameObject> enemies;

    // Use this for initialization
    void Start () {
        inst = this;
        spawnLine1 = GameObject.Find("SpawnLine1").transform.position;
        spawnLine2 = GameObject.Find("SpawnLine2").transform.position;
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    internal void spawnEnemy(int enemyType)
    {
        GameObject enemy = GameObject.Instantiate(enemyPrefabs[enemyType]);
        enemy.GetComponent<EnemyDancer>().enemyType = enemyType;
        enemy.transform.position = spawnLine1;
        enemy.transform.position += UnityEngine.Random.Range(0f, 1f) * (spawnLine2 - spawnLine1);
        enemies.Add(enemy);
    }
}
