﻿using UnityEngine;
using System.Collections;

public class BulletPlayer : MonoBehaviour {

    float lifeTime = 0.5f;
    public Vector3 velocity;

    Rigidbody rb;

    void Awake()
    {
        rb = gameObject.transform.GetComponent<Rigidbody>();
    }

    void Start()
    {


        rb.AddForce(new Vector3(5, 0, 0));
        StartCoroutine(CountLife());
    }

    public void AddForce(Vector3 vel)
    {
        rb.AddForce(vel);
    }

    IEnumerator CountLife()
    {
        yield return new WaitForSeconds(lifeTime);
        Destroy(this.gameObject);

    }

    Vector3 direction;

    public void SetDirection(Vector3 _direction)
    {
        direction = _direction;
    }

    public float speed = 5f;

    void Update()
    {

        transform.position += direction * speed * Time.deltaTime;
        // transform.localScale *= 1.01f;
    }




    public float damage = 10f;

    [SerializeField]
    GameObject hitMarker;

    void OnTriggerEnter(Collider coll)
    {
        //  Vector3 pos;
        // pos = coll.transform.position;
        // pos.y = 1.5f;
        // GameObject _hitMarker = Instantiate(hitMarker, pos, Quaternion.identity) as GameObject;
        Debug.Log("coll!");
        IDamagble d = coll.GetComponent<IDamagble>();
        if (d != null) coll.GetComponent<IDamagble>().InflictDamage(damage);


    }
}
