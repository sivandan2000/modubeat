﻿using UnityEngine;
using System.Collections;

public class DeathSprite : MonoBehaviour {

    [SerializeField]
    float disapearTime = 0.5f;

	IEnumerator Start () {

        yield return new WaitForSeconds(disapearTime);
        Destroy(this.gameObject);
	
	}
	
}
